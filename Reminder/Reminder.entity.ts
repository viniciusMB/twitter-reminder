import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { UserEntity } from '../User/User.entity';

@Entity()
export class ReminderEntity {
  @PrimaryGeneratedColumn()
  reminderID: string;

  @Column()
  reminderTitle: string;

  @Column('text')
  reminderContent;

  @Column('datetime')
  date;

  @ManyToOne(() => UserEntity, (user) => user.reminders)
  user: UserEntity;
}

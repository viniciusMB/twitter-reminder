import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ReminderService } from './Reminder.service';
import { ReminderController } from './Reminder.controller';
import { ReminderEntity } from './Reminder.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ReminderEntity])],
  providers: [ReminderService],
  controllers: [ReminderController],
})
export class ReminderModule {}

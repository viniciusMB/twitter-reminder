import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import { UserEntity } from './User/User.entity';
import { NotesEntity } from './Notes/Notes.entity';
import { ReminderEntity } from './Reminder/Reminder.entity';

import { CronService } from './cron/cron.service';

import { ReminderModule } from './Reminder/Reminder.module';
import { ReminderService } from './Reminder/Reminder.service';

@Module({
  imports: [
    ReminderModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: 'mariadb',
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT) || 3306,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [UserEntity, NotesEntity, ReminderEntity],
        synchronize: true,
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService, CronService, ReminderService],
})
export class AppModule {}

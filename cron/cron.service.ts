import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

import { ReminderService } from 'src/Reminder/Reminder.service';
import { ReminderEntity } from 'src/Reminder/Reminder.entity';

@Injectable()
export class CronService {
  constructor(
    @Inject(forwardRef(() => ReminderService))
    private readonly reminderService: ReminderService
  ) {}
  @Cron(CronExpression.EVERY_MINUTE)
  async runEveryMinute() {
    console.log('Checking reminders...');
    const now = new Date();
    const result: ReminderEntity[] = await this.reminderService.findByDate(now);
    if (result.length > 0) {
      return console.log('finded');
    }
  }
}

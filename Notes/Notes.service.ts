import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateNotesDto } from './dto/CreateNotes.dto';
import { NotesEntity } from './Notes.entity';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(NotesEntity)
    private NotesRepository: Repository<NotesEntity>
  ) {}

  findAll() {
    return this.NotesRepository.find();
  }

  findOne(id: string) {
    return this.NotesRepository.findOne(id);
  }

  remove(id: string) {
    return this.NotesRepository.delete(id);
  }

  register(createNotesDto: CreateNotesDto) {
    return this.NotesRepository.create(createNotesDto);
  }
}

export class CreateNotesDto {
  readonly noteTitle: string;
  readonly noteContent: string;
  readonly userUserID: string;
}

import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { UserEntity } from '../User/User.entity';

@Entity()
export class NotesEntity {
  @PrimaryGeneratedColumn()
  noteID: string;

  @Column()
  noteTitle: string;

  @Column('text')
  noteContent;

  @ManyToOne(() => UserEntity, (user) => user.notes)
  user: UserEntity;
}

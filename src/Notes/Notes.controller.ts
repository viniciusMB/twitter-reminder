import { Controller, Get, Param, Post, Body, Delete } from '@nestjs/common';
import { CreateNotesDto } from './dto/CreateNotes.dto';
import { NotesEntity } from './Notes.entity';
import { NotesService } from './Notes.service';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}
  @Get()
  findAll(): Promise<NotesEntity[]> {
    return this.notesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<NotesEntity> {
    return this.notesService.findOne(id);
  }

  @Post('new')
  create(@Body() createNotesDto: CreateNotesDto) {
    return this.notesService.register(createNotesDto);
  }

  @Delete('delete')
  remove(@Param('id') id: string) {
    return this.notesService.remove(id);
  }
}

export class CreateReminderDto {
  readonly reminderTitle: string;
  readonly reminderContent: string;
  readonly userUserID: string;
  date: Date;
}

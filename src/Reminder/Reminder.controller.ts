import { Controller, Get, Param, Post, Body, Delete } from '@nestjs/common';
import { CreateReminderDto } from './dto/CreateReminder.dto';
import { ReminderEntity } from './Reminder.entity';
import { ReminderService } from './Reminder.service';

@Controller('remind')
export class ReminderController {
  constructor(private readonly reminderService: ReminderService) {}
  @Get()
  async findAll(): Promise<ReminderEntity[]> {
    return await this.reminderService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<ReminderEntity> {
    return await this.reminderService.findOne(id);
  }

  @Post('new')
  create(@Body() createReminderDto: CreateReminderDto) {
    //if (@Is(createReminderDto.date))
    createReminderDto.date = new Date(createReminderDto.date);
    return this.reminderService.register(createReminderDto);
  }

  @Delete('delete')
  async remove(@Param('id') id: string) {
    return await this.reminderService.remove(id);
  }
}

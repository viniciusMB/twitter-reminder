import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReminderDto } from './dto/CreateReminder.dto';
import { ReminderEntity } from './Reminder.entity';
import { ReminderModule } from './Reminder.module';

@Injectable()
export class ReminderService {
  constructor(
    @InjectRepository(ReminderEntity)
    private ReminderRepository: Repository<ReminderEntity>
  ) {}

  findAll() {
    return this.ReminderRepository.find();
  }

  findOne(id: string) {
    return this.ReminderRepository.findOne(id);
  }

  remove(id: string) {
    return this.ReminderRepository.delete(id);
  }

  register(createReminderDto: CreateReminderDto) {
    return this.ReminderRepository.create(createReminderDto);
  }

  async findByDate(date: Date): Promise<ReminderEntity[]> {
    const remindersAndQuantity = await this.ReminderRepository.findAndCount({
      where: { date: date },
    });
    const reminders = remindersAndQuantity[0];
    let quantity: number = remindersAndQuantity[1];

    let usersArray: Promise<ReminderEntity[]>;

    for (quantity; quantity == 0; quantity--) {
      usersArray = this.ReminderRepository.find({
        where: {
          reminder: { reminderID: reminders[quantity].reminderID },
        },
        relations: ['user'],
      });
    }

    return usersArray;
  }
}
export { ReminderModule };

## Requisitos

- [ ] Utilizar o nest

- [ ] Crud do usuário

- [ ] Crud de lembretes

- [ ] Lembrar
    - [ ] Email
    - [ ] Twitter

- [ ] Swagger

- [ ] Testes
    - [ ] Unitários
    - [ ] Integração
    - [ ] 100% de coverage

- [ ] Cache com Redis

- [ ] Docker

- [ ] Typeorm
    - [ ] Relacionamentos
    - [ ] Usuário 1:N Lembretes
    - [ ] Usuário 1:N Notas 

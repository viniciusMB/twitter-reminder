import { Controller, Get, Param, Post, Body, Delete } from '@nestjs/common';
import { CreateUserDto } from './dto/CreateUser.dto';
import { UserEntity } from './User.entity';
import { UserService } from './User.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get()
  findAll(): Promise<UserEntity[]> {
    return this.userService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<UserEntity> {
    return this.userService.findOne(id);
  }

  @Post('signIn')
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.register(createUserDto);
  }

  @Delete('delete')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }
}

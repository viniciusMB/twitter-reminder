import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';

import bcrypt from 'bcrypt';
import { ReminderEntity } from '../Reminder/Reminder.entity';
import { NotesEntity } from '../Notes/Notes.entity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  userID: number;

  @Column()
  email: string;

  @Column()
  name: string;

  @OneToMany(() => ReminderEntity, (reminder) => reminder.user)
  reminders: ReminderEntity[];

  @OneToMany(() => NotesEntity, (notes) => notes.user)
  notes: NotesEntity[];

  @Column()
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }
}
